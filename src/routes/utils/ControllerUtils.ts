export default class ControllerUtils {
    static parseNumberParam(param: string): number | null {
        if (isNaN(parseInt(param)))
            return null;
        return parseInt(param)
    }
}