export default class Obfuscation {
    // Convertit en base 64;
    static obfuscate(str: string): string {
        return Buffer.from(str, 'binary').toString('base64');
    }
    // Decode un base 64
    static deobfuscate(str: string): string {
        return Buffer.from(str, 'base64').toString('binary');
    }

}