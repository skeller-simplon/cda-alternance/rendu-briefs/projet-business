import express from "express";
import ExpenseBusiness from "../business/ExpenseBusiness"
import ControllerUtils from "./utils/ControllerUtils"
const ExpenseController = express.Router();
const _expenseBusiness = new ExpenseBusiness();

ExpenseController.post("/", (req, res) => {
    try {
        let amount = req.body.amount
        let payerId = req.body.payerId
        let groupId = req.body.groupId
        _expenseBusiness.addExpense(amount, payerId, groupId).then(expense => {
            res.send(expense)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})
ExpenseController.post("/:expenseId/cancel", (req, res) => {
    try {
        let expenseId = ControllerUtils.parseNumberParam(req.params.expenseId)
        if (expenseId === null)
            throw new Error("Incorrect parameter 'expenseId'")
        _expenseBusiness.cancelExpense(expenseId).then(expense => {
            res.send(expense)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})


export default ExpenseController;