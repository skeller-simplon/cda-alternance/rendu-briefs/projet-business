import express from "express";
import UserBusiness from "../business/UserBusiness"
import { Group } from "../entity/Group";
import ControllerUtils from "./utils/ControllerUtils";

const UserController = express.Router();
const _userBusiness = new UserBusiness();

UserController.get("/:userId/groups", (req, res) => {

    try {
        let userId = ControllerUtils.parseNumberParam(req.params.userId);
        _userBusiness.getUserGroups(userId).then((groups: Group[]) => {
            res.send(groups)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

export default UserController;