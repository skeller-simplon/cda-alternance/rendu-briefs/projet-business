import express from "express";
import GroupBusiness from "../business/GroupBusiness"
import ControllerUtils from "./utils/ControllerUtils";
import Obfuscation from "./utils/Obfuscation";

const GroupController = express.Router();
const _groupBusiness = new GroupBusiness();

GroupController.route("/").post((req, res) => {
    let users = req.body.users
    try {
        _groupBusiness.createGroup(users).then(group => {
            res.send(group)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

GroupController.get("/:groupId/balance", (req, res) => {
    let groupId = ControllerUtils.parseNumberParam(req.params.groupId);
    try {
        _groupBusiness.getGroupBalance(groupId).then(balance => {
            res.send(balance)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

GroupController.post("/:groupId/join", (req, res) => {
    let groupId = ControllerUtils.parseNumberParam(req.params.groupId);
    let userId = req.body.userId
    try {
        _groupBusiness.addUserToGroup(userId, groupId).then(group => {
            res.send(group)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

GroupController.post("/:groupId/settle", (req, res) => {
    let groupId = ControllerUtils.parseNumberParam(req.params.groupId);

    try {
        _groupBusiness.settleGroup(groupId).then(group => {
            res.send(group)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

GroupController.post("/:groupId/validate", (req, res) => {
    let groupId = ControllerUtils.parseNumberParam(req.params.groupId);

    try {
        _groupBusiness.validateGroup(groupId).then(group => {
            res.send(group)
        })
    } catch (error) {
        res.status(400).send(error)
    }
})

GroupController.get("/:groupId/inviteLink", (req, res) => {
    res.json({ link: `http://application.co/join?link=${Obfuscation.obfuscate(req.params.groupId)}` })
})

export default GroupController;