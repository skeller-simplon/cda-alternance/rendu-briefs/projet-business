import express from 'express'
import GroupController from './GroupController';
import UserController from './UserController';
import ExpenseController from './ExpenseController';

const router = express()

router.use("/group", GroupController);
router.use("/user", UserController);
router.use("/expense", ExpenseController);

export default router
