import { User } from "../entity/User";
import UserBusiness from "../business/UserBusiness"
import GroupRepository from "../repositories/GroupRepository";
import { Group } from "../entity/Group";
import { UpdateResult } from "typeorm";
import ExpensesCalc from "./helper/ExpensesCalc"

export default class GroupBusiness {
    _userBusiness: UserBusiness = new UserBusiness();
    _groupRepository: GroupRepository = new GroupRepository();

    createGroup(users: string): Promise<Group> {
        // Traite un tableau stringifié ("['a', 'b']")
        let parsedUsers: string[] = users.split(",").map(user => user.replace(/'|\[|\]/g, ''))

        return this._userBusiness.retrieveUserListFromNames(parsedUsers).then(users => {
            return this._groupRepository.createGroup(users)
        })
    }

    async addUserToGroup(userId: number, groupId: number) {
        this._userBusiness.getUserById(userId).then((user: User) => {
            return this._groupRepository.addUserToGroup(groupId, user)
        })
    }

    getGroupById(groupId: number): Promise<Group> {
        return this._groupRepository.getGroupById(groupId)
    }

    async getGroupBalance(groupId: any) {
        let group = await this._groupRepository.getGroupByIdWithRelations(groupId)
        return new ExpensesCalc().getBalance(group.members)
    }

    settleGroup(groupId: number): Promise<UpdateResult> {
        return this._groupRepository.settleGroup(groupId)
    }

    validateGroup(groupId: number): Promise<UpdateResult> {
        return this._groupRepository.validateGroup(groupId)
    }


    manageOperationsOnInactiveGroups(group: Group) {
        if (group.isSettled || group.isValidated)
            throw new Error("Impossible to add expenses on a closed group.")
    }

}