import { Expense } from "../entity/Expense";
import { Group } from "../entity/Group";
import ExpenseRepository from "../repositories/ExpenseRepository";
import GroupBusiness from "./GroupBusiness";
import UserBusiness from "./UserBusiness";

export default class ExpenseBusiness {

    private _repo: ExpenseRepository = new ExpenseRepository()
    private _userBusiness = new UserBusiness();
    private _groupBusiness = new GroupBusiness();

    async addExpense(amount: number, payerId: number, groupId: number): Promise<Expense> {
        let user = await this._userBusiness.getUserById(payerId)
        let group = await this._groupBusiness.getGroupById(groupId)
        this._groupBusiness.manageOperationsOnInactiveGroups(group)

        return this._repo.addExpense(amount, user, group)
    }

    async cancelExpense(expenseId: number) {
        let expense = await this._repo.getExpenseById(expenseId)
        let group = await this._groupBusiness.getGroupById(expense.group.id)
        this._groupBusiness.manageOperationsOnInactiveGroups(group)

        return this._repo.cancelExpense(expenseId)
    }


}