import { Group } from "../entity/Group";
import { User } from "../entity/User";
import UserRepository from "../repositories/UserRepository"

export default class UserBusiness {
    private _repo: UserRepository = new UserRepository()

    public async retrieveUserListFromNames(usernames: string[]) {
        let userList: User[] = [];
        for (const username of usernames) {
            let user: User = await this._repo.getUserByName(username)
                .then(user => {
                    if (!user) {
                        return this._repo.createUser(username);
                    }
                    return user
                });
            userList.push(user)
        }
        return userList
    }
    getUserById(userId: number): Promise<User> {
        return this._repo.getUserById(userId)
    }
    getUserGroups(userId: number): Promise<Group[]> {
        return this._repo.getUserGroups(userId)
    }

}