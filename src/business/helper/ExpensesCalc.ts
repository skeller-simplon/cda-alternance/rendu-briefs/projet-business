import { Expense } from "../../entity/Expense";
import { User } from "../../entity/User"

interface IExpense {
    user: { id: number, name: string },
    expense: Expense
}

interface IUserBalance {
    user: User,
    balance: number,
    messages: string[]
}

export default class ExpensesCalc {

    /**
     * Calcule les transferts que devront faire des utilisateurs afin d'équilibrer leur comptes.
     * @param users List d'utilisateurs
     */
    getBalance(users: User[]) {
        // Récupère un tableau de toutes les dépenses en y incluant l'utilisateur à chaque fois.
        // {User: {}, expenses: []}
        let usersExpenses: IExpense[] = [].concat.apply([], users.map(user =>
            user.expenses.map(expense => {
                return {
                    user: { id: user.id, name: user.name },
                    expense: expense
                }
            })
        ))
        // Somme totale des dépences.
        let total: number = usersExpenses.map(exp => exp.expense.amount).reduce((a, b) => a + b, 0)
        // Part que chacun devra payer.
        let userShare: number = total / users.length;

        let balancesArray: IUserBalance[] = []
        // On parcours les utilisateurs afin de définir pour chacun leur balances.
        for (const user of users) {
            // La balance pour chaque utilisateur peut ce calculer par frais avancés - part de chacun.
            balancesArray.push({ user: user, balance: user.expenses.map(e => e.amount).reduce((a, b) => a + b, 0) - userShare, messages: [] })
        }

        // Boucle jusqu'à que toutes les balances soient === 0.
        while (balancesArray.map(b => b.balance).filter(x => x === 0).length !== balancesArray.length)
            for (const userBalance of balancesArray) {
                // On trouve quelqu'un devant de l'argent, sa balance est > 0.
                if (userBalance.balance > 0) {
                    // On lui adjoint un receiver dont la balance est < 0
                    let receiver = balancesArray.find((b => {
                        return b.user.id !== userBalance.user.id && b.balance < 0
                    }))

                    // Si la somme des deux est > 0, on transfert la balance de l'utilisateur.
                    if (userBalance.balance + receiver.balance <= 0) {
                        this.transfer(userBalance, receiver, userBalance.balance);
                    }
                    // Sinon, celle du receiver.
                    else {
                        this.transfer(userBalance, receiver, receiver.balance * -1);
                    }
                }
            }
        return balancesArray
    }

    private transfer(userBalance: IUserBalance, receiver: IUserBalance, transfer: number) {
        userBalance.balance -= transfer;
        receiver.balance += transfer;
        let msg = this.getTransferMessage(userBalance.user, receiver.user, transfer)
        userBalance.messages.push(msg);
        receiver.messages.push(msg);
    }

    private getTransferMessage(sender: User, receiver: User, amount: number): string {
        return `${sender.name} doit ${amount}PO à ${receiver.name}`
    }

}
