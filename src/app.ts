import express from 'express';
import "reflect-metadata";
import bodyParser from "body-parser";
import cors from "cors";
import routes from "./routes/routes"
import db from './repositories/db';

const app = express();
const port = 3000;

db.init().then(() => {
    app.get('/', (req, res) => {
        res.send('Hello world');
    });


    app.listen(port, () => {
        return console.log(`server is listening on ${port}`);
    });

    // Bodyparser - traite les requêtes.
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: '50mb' }));

    app.use(cors({ origin: "*", methods: "*" }))

    app.use("/api", routes);
});
