import { group } from "console";
import { getConnection, UpdateResult } from "typeorm";
import { Group } from "../entity/Group";
import { User } from "../entity/User";


export default class GroupRepository {
    private _repo = () => getConnection().getRepository(Group)

    createGroup(users: User[]) {
        let repo = this._repo();
        return repo.save(repo.create({ members: users, isSettled: false, isValidated: false }))
    }

    addUserToGroup(groupId: number, user: User) {
        let repo = this._repo();
        return repo.findOne(groupId, { relations: ["members"] }).then(group => {
            group.members.push(user)
            return repo.save(group)
        })
    }

    getGroupById(groupId: number): Promise<Group> {
        return this._repo().findOne(groupId)
    }

    getGroupByIdWithRelations(groupId: number): Promise<Group> {
        return this._repo().findOne(groupId, { relations: ["members", "members.expenses"] })
    }


    settleGroup(groupId: number): Promise<UpdateResult> {
        return this._repo().update(groupId, { isSettled: true })
    }
    validateGroup(groupId: number): Promise<UpdateResult> {
        return this._repo().update(groupId, { isValidated: true })
    }

}