import { getConnection } from "typeorm";
import { Expense } from "../entity/Expense";
import { Group } from "../entity/Group";
import { User } from "../entity/User";


export default class ExpenseRepository {
    private _repo = () => getConnection().getRepository(Expense)

    getExpenseById(expenseId: number) {
        return this._repo().findOne(expenseId, { relations: ["group"] })
    }
    addExpense(amount: number, payer: User, group: Group) {
        let repo = this._repo()
        return repo.save(repo.create({ amount: amount, payer: payer, group: group }))
    }

    async cancelExpense(expenseId: number) {
        return this._repo().update(expenseId, { isCancelled: true });
    }
}