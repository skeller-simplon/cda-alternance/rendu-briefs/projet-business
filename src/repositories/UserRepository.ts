import { getConnection } from "typeorm";
import { Group } from "../entity/Group";
import { User } from "../entity/User";

export default class UserRepository {
    private _repo = () => getConnection().getRepository(User)

    createUser(username: string): Promise<User> {
        let repo = this._repo();
        return repo.save(repo.create({ name: username }))
    }

    getUserByName(username: string): Promise<User> {
        return this._repo().findOne({ where: [{ name: username }] });
    }

    getUserById(userId: number, includeGroups: boolean = false): Promise<User> {
        return this._repo().findOne(userId, { relations: includeGroups ? ["groups"] : [] })
    }

    getUserGroups(userId: number): Promise<Group[]> {
        return this.getUserById(userId, true).then(user => {
            return user.groups
        })
    }
}