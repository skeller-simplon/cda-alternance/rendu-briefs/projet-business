import { Connection, createConnection } from 'typeorm';
import { Expense } from '../entity/Expense';
import { Group } from '../entity/Group';
import { User } from '../entity/User';
import dotenv from "dotenv";
dotenv.config()


export default class db {
    static init() {
        return createConnection({
            name: "default",
            type: "mysql",
            host: "localhost",
            username: process.env.SQL_USERNAME,
            password: process.env.SQL_PASSWORD,
            database: process.env.SQl_DATABASE_NAME,
            entities: [
                Expense,
                Group,
                User
            ],
            synchronize: true,
            logging: false
        })
    }
}