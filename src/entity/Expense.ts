import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Group } from "./Group";
import { User } from "./User";

@Entity()
export class Expense {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    amount: number;

    @ManyToOne(() => User, user => user.expenses)
    payer: User;

    @ManyToOne(() => Group, user => user.expenses)
    group: User;

    @Column({ default: false })
    isCancelled: boolean;
}
