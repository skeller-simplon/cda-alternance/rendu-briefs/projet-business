import { group } from "console";
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinTable, RelationCount } from "typeorm";
import { Expense } from "./Expense";
import { Group } from "./Group";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(() => Expense, expense => expense.payer)
    expenses: Expense[];

    @ManyToMany(() => Group, group => group.members)
    groups: Group[];
}
